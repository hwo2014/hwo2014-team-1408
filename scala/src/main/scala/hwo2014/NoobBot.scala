package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec
import scala.collection.mutable

object NoobBot extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new NoobBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class NoobBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = Serialization.formats(NoTypeHints)//new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  var counter = 0
  
  //val trackInfo = new TrackInfo()
  private var _myCarId: CarId = _
  def myCarId = _myCarId
  val myCarInfo = new CarInfo
  val carInfoMap = mutable.Map[CarId, CarInfo]()

  send(MsgSeq("join", Join(botName, botKey)))
  play()
  myCarInfo.writeToCSV("mycar_log.csv")

  @tailrec private def play() {
    val line = reader.readLine()
    if (line != null) {
      counter += 1
      val msg = Serialization.read[MsgSeq](line)
      //categorize(msg)
      msg.msgType match {
        case "yourCar" =>
          _myCarId = msg.data.extract[CarId]
          carInfoMap += myCarId -> myCarInfo
        case "gameInit" =>
          println("gameInit...")
          gameInit(msg)
        case "carPositions" =>
          carPositions(msg)
        case _ =>
          println(msg.msgType)
//        case "yourCar" =>
//        case "lapFinished" =>
//        case "finish" =>
      }

      play()
    }
  }

  private def gameInit(msg: MsgSeq) {
    val race = msg.data.extract[GameInit.GameInit].race
    val track = race.track
    
    TrackInfo.init(track)

    println(race.cars)
    race.cars.filter(_.id != myCarId) foreach {
      carInfoMap += _.id -> new CarInfo
    }
    println(carInfoMap)
  }

  private def carPositions(msg: MsgSeq) {
    val carList = msg.data.extract[List[CarPositions.Car]]
    carList.foreach( car => {
      carInfoMap(car.id).update(car.angle, car.piecePosition)
    })

    print("["+ msg.gameTick.values +"]")
    println(s"$myCarInfo ${TrackInfo.pieces(myCarInfo.index)}")

    setThrottle(0.3)
  }

  def setThrottle(throttle: Double) {
    myCarInfo.throttles += throttle

    send(MsgSeq("throttle", throttle))
  }

  def send(msg: MsgSeq) {
    writer.println(Serialization.write(msg))
    writer.flush()
  }
}

object TrackInfo {
  import mutable.ListBuffer

  implicit val formats = Serialization.formats(NoTypeHints)
  
  private var _id: String = _
  private var _name: String = _
  private var _pieces: ListBuffer[_ <: Piece] = _
  private var _lanes: Array[Int] = _

  def id = _id
  def name = _name
  def pieces = _pieces
  def lanes = _lanes

  def init(track: GameInit.Track) {
    _id = track.id
    _name = track.name
    _pieces = (track.pieces map { n => n match {
        case JObject(List(("length", _))) => n.extract[Line]
        case JObject(List(("length", _), _*)) => n.extract[Line]
        case _ => n.extract[Arc]
      }
    }).to[ListBuffer]
    _lanes = track.lanes.map(_.distanceFromCenter * -1).to[Array]
  }
}

class CarInfo {
  import mutable.ListBuffer

  private val angles = ListBuffer[Double]()
  private val positions = ListBuffer[CarPositions.Position]()

  def angle = angles.lastOption.getOrElse(0.0)
  def position = positions.lastOption.getOrElse(CarPositions.Position())

  private val velocs = ListBuffer[Double]()
  private val accels = ListBuffer[Double]()

  def veloc = velocs.lastOption.getOrElse(0.0)
  def accel = accels.lastOption.getOrElse(0.0)

  private val angVels = ListBuffer[Double]()
  private val angAccs = ListBuffer[Double]()

  def angVel = angVels.lastOption.getOrElse(0.0)
  def angAcc = angAccs.lastOption.getOrElse(0.0)

  def index = position.pieceIndex
  def distance = position.inPieceDistance

  val throttles = ListBuffer[Double]()

  def update(angle: Double, position: CarPositions.Position) {
    val da = angle - this.angle
    val dav = da - angVel
    angVels += da
    angAccs += dav

    val dx = if (position.pieceIndex == this.position.pieceIndex) {
      position.inPieceDistance - this.position.inPieceDistance
    } else {
      position.inPieceDistance - this.position.inPieceDistance +
        (TrackInfo.pieces(this.position.pieceIndex) match {
          case p: Line => p.length
          case p: Arc =>
            math.Pi / 180.0 * (
              p.radius * p.angle.abs +
              TrackInfo.lanes(this.position.lane.startLaneIndex) * p.angle
            )
        })
    }
    val dv = dx - veloc
    velocs += dx
    accels += dv
    
    angles += angle
    positions += position
  }

  def writeToCSV(fileName: String) {
    val table = List(velocs, accels, angVels, angAccs, throttles)

    // 転置かつ文字列変換
    val transposed = (0 until table.map(_.size).max).map {
      i => table.map(r => { if (r.isDefinedAt(i)) r(i).toString else "" })
    }.toList

    // ヘッダを連結し，カンマ区切りに変換
    val lines = (List(
      "velocity",
      "acceleration",
      "angular velocity",
      "angular acceleration",
      "throttle"
    ) :: transposed).map(_.mkString(","))

    val out = new java.io.PrintWriter(
      "%tY%<tm%<td%<tH%<tM".format(new java.util.Date) + s"_$fileName"
    )
    out.flush()
    lines.foreach(out.println)
    out.close()
  }

  override def toString = f"v:$veloc%.3f a:$accel%.3f "+
                          f"av:$angVel%.3f aa:$angAcc%.3f "+
                          f"d:$distance%.3f id:$index"
}

case class CarId(name: String, color: String)

trait Piece //(arr: List[JValue])
case class Line(length: Double, switch: Boolean = false) extends Piece
case class Arc(radius: Double, angle: Double) extends Piece

object GameInit {
  case class GameInit(race: Race)
  case class Race(track: Track, cars: List[Car], raceSession: JValue)
  case class Track(id: String, name: String, pieces: List[JObject], lanes: List[Lane], startingPoint: JValue)
  case class Lane(distanceFromCenter: Int, index: Int)
  case class Car(id: CarId, dimensions: JValue)
}

object CarPositions {
  case class Car(id: CarId, angle: Double, piecePosition: Position)
  case class Position(pieceIndex: Int = 0, inPieceDistance: Double = 0.0, lane: Lane = Lane(0,0), lap: Int = 0)
  case class Lane(startLaneIndex: Int, endLaneIndex:Int)
}

case class Join(name: String, key: String)
case class MsgSeq(msgType: String, data: JValue, gameId: JValue, gameTick: JValue)
object MsgSeq {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgSeq = {
    MsgSeq(msgType, Extraction.decompose(data), JNothing, JNothing)
  }
}
